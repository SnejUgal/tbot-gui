use crate::messaging::{Method, Request, Response};
use gio::prelude::*;
use glib::{Continue, Receiver};
use gtk::{
    prelude::*, Align, Application, ApplicationWindowBuilder, BoxBuilder,
    ButtonBuilder, ComboBoxTextBuilder, EntryBuilder, GridBuilder,
    HeaderBarBuilder, InputPurpose, Justification, LabelBuilder, Orientation,
    TextViewBuilder, WindowPosition,
};
use std::{cell::RefCell, env::args, rc::Rc};
use tokio::sync::watch::Sender;

const GET_ME: &str = "getMe";
const GET_WEBHOOK_INFO: &str = "getWebhookInfo";

fn build_ui(
    application: &Application,
    sender: Rc<RefCell<Sender<Option<Request>>>>,
    receiver: Rc<RefCell<Option<Receiver<Response>>>>,
) {
    let window = ApplicationWindowBuilder::new()
        .application(application)
        .title("tbot GUI")
        .window_position(WindowPosition::Center)
        .build();

    let header_bar = HeaderBarBuilder::new()
        .title("tbot GUI")
        .show_close_button(true)
        .build();
    window.set_titlebar(Some(&header_bar));

    let main_row = BoxBuilder::new()
        .border_width(16)
        .spacing(16)
        .halign(Align::Fill)
        .valign(Align::Fill)
        .orientation(Orientation::Horizontal)
        .build();
    window.add(&main_row);

    let configuration = GridBuilder::new()
        .row_spacing(16)
        .column_spacing(16)
        .valign(Align::Start)
        .hexpand(false)
        .build();
    main_row.pack_start(&configuration, false, false, 0);

    let token_label = LabelBuilder::new()
        .label("Bot token")
        .justify(Justification::Right)
        .halign(Align::End)
        .build();
    configuration.attach(&token_label, 0, 0, 1, 1);

    let token_field = Rc::new(
        EntryBuilder::new()
            .visibility(false)
            .input_purpose(InputPurpose::Password)
            .width_chars(40)
            .halign(Align::Start)
            .build(),
    );
    configuration.attach(&*token_field, 1, 0, 1, 1);

    let method_label = LabelBuilder::new()
        .label("Method")
        .justify(Justification::Right)
        .halign(Align::End)
        .build();
    configuration.attach(&method_label, 0, 1, 1, 1);

    let method_select =
        Rc::new(ComboBoxTextBuilder::new().halign(Align::Start).build());
    method_select.append(Some(GET_ME), GET_ME);
    method_select.append(Some(GET_WEBHOOK_INFO), GET_WEBHOOK_INFO);
    method_select.set_active_id(Some(GET_ME));
    configuration.attach(&*method_select, 1, 1, 1, 1);

    let button = Rc::new(
        ButtonBuilder::new()
            .label("Send")
            .halign(Align::Start)
            .build(),
    );
    configuration.attach(&*button, 1, 2, 1, 1);

    let token_on_send = Rc::clone(&token_field);
    let method_on_send = Rc::clone(&method_select);
    let button_on_send = Rc::clone(&button);
    button.connect_clicked(move |_| {
        token_on_send.set_sensitive(false);
        method_on_send.set_sensitive(false);
        button_on_send.set_sensitive(false);

        let token = token_on_send.get_text().unwrap().as_str().to_owned();
        let method = match method_on_send.get_active_id().unwrap().as_str() {
            GET_ME => Method::GetMe,
            GET_WEBHOOK_INFO => Method::GetWebhookInfo,
            _ => unreachable!(),
        };

        sender
            .borrow_mut()
            .broadcast(Some(Request { token, method }))
            .unwrap();
    });

    let response_view = TextViewBuilder::new()
        .monospace(true)
        .editable(false)
        .width_request(256)
        .hexpand(true)
        .halign(Align::Fill)
        .valign(Align::Fill)
        .build();
    main_row.pack_end(&response_view, true, true, 0);

    receiver
        .borrow_mut()
        .take()
        .unwrap()
        .attach(None, move |response| {
            token_field.set_sensitive(true);
            method_select.set_sensitive(true);
            button.set_sensitive(true);

            let response = format!("{:#?}", response);
            response_view.get_buffer().unwrap().set_text(&response);
            Continue(true)
        });

    window.show_all();
}

pub fn init(sender: Sender<Option<Request>>, receiver: Receiver<Response>) {
    let sender = Rc::new(RefCell::new(sender));
    let receiver = Rc::new(RefCell::new(Some(receiver)));
    let application =
        Application::new(Some("rs.tbot.gui"), Default::default()).unwrap();

    application.connect_activate(move |application| {
        build_ui(application, Rc::clone(&sender), Rc::clone(&receiver))
    });

    application.run(&args().collect::<Vec<_>>());
}
