use crate::messaging::{Method, Request, Response};
use glib::Sender;
use std::sync::Arc;
use tbot::Bot;
use tokio::sync::watch::Receiver;

#[tokio::main]
pub async fn start(
    sender: Sender<Response>,
    mut receiver: Receiver<Option<Request>>,
) {
    let sender = Arc::new(sender);

    while let Some(message) = receiver.recv().await {
        let message = match message {
            Some(message) => message,
            None => continue,
        };

        tokio::spawn(make_request(Arc::clone(&sender), message));
    }
}

async fn make_request(sender: Arc<Sender<Response>>, message: Request) {
    let bot = Bot::new(message.token);

    let result: Response = match message.method {
        Method::GetMe => {
            let result = bot.get_me().call().await;
            Arc::new(result)
        }
        Method::GetWebhookInfo => {
            let result = bot.get_webhook_info().call().await;
            Arc::new(result)
        }
    };

    sender.send(result).unwrap();
}
