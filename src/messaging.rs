use std::{fmt::Debug, sync::Arc};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
pub enum Method {
    GetMe,
    GetWebhookInfo,
}

#[derive(Debug, PartialEq, Eq, Clone, Hash)]
pub struct Request {
    pub token: String,
    pub method: Method,
}

pub type Response = Arc<dyn Debug + Send + Sync>;
