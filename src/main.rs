use glib::MainContext;
use std::thread::spawn;
use tokio::sync::watch::channel;

mod gui;
mod messaging;
mod request_thread;

fn main() {
    let (request_sender, request_receiver) = channel(None);
    let (response_sender, response_receiver) =
        MainContext::channel(glib::Priority::default());

    let tokio =
        spawn(move || request_thread::start(response_sender, request_receiver));

    gui::init(request_sender, response_receiver);
    tokio.join().unwrap();
}
